#include <iostream>
using namespace std;

struct node {
	int data;
	node *next;
};

bool isEmpty(node *head);
char menu();
void insertAsFirstElement(node *&head, node *&last, int data);
void insert(node *&head, node *&last);
void remove(node *&head, node *&last, int data);
void showList(node *current);
int fibo(unsigned long long int fib[100]);
int *collatz(int z);
static unsigned long long int fib[100];


bool isEmpty(node *head) {
	if (head == NULL)
		return true;
	else 
		return false;
}

char menu() {
	char choice; 
	cout << "\t*****************************************\n";
	cout << "\t\t[1] Insert data here.\n";
	cout << "\t\t[2] Remove data.\n";
	cout << "\t\t[3] Display data.\n";
	cout << "\t\t[4] Exit.\n";
	cout << "\t*****************************************\n";
	
	cout << "Enter your choice:";
	cin >> choice;
	return choice;
}

void insertAsFirstElement(node *&head, node *&last, int data) {
	node *temp = new node;
	temp->data = data; 
	temp->next = NULL;
	head = temp; 
	last = temp; 
}

void insert(node *&head, node *&last) {
	system("CLS");
	int data;
	cout << "Enter a given data:";
	cin >> data;
	system("CLS");
	if (isEmpty(head))
		insertAsFirstElement(head, last, data);
		else {
			node *temp = new node;
			temp->data = data; 
			temp->next = NULL;
			last->next = temp;
			last = temp; 
		}
} 

void remove(node *&head, node *&last) {
	if(isEmpty(head))
		cout << "The list is empty! \n" << endl;
		system("pause");
		system("CLS");
		
	if (head == last) {
		delete head; 
		head == NULL;
		last == NULL;
	} 
	else {
		node *temp = head;
		head = head->next;
		delete temp;
	}
} 

void showList(node *current) {
	system("cls");
	if(isEmpty(current)) 
		cout << "The list is empty!\n" << endl;	
	else {
		system("CLS");
		cout << "==========DISPLAYING FIBONACCI (from collatz)=================" << endl;
		while (current != NULL) {
			cout << ' ' << current->data << endl;
			current = current->next;
		} 
		cout << "=============================================" << endl;
		
		
		system("pause");
		system("CLS");
	}
} 

int fibo(unsigned long long int fib[100]) {
	unsigned long long int n=91, n1=0, n2=1, nxt;
	for (int i=1; i < 91; i++) {
    if (i <= 1) {  	
    	nxt = i;
    	fib[i]=1;
	}
    else {
        nxt = n1+n2;
        n1 = n2;
        n2 = nxt;
    	fib[i] = n2;
    }
}
	return fib [1000];
}

int *collatz(int z) {
	int x = 0;
	static int array[1000];
	while(z != 1) {
		if(z%2 != 0) {
			z = z*3 +1;
			array[x] = z;
		}
		else {
			z = z/2;
			array[x]= z;
		}
		x++;
	}
	return array;
}


int main () {
	int z;
	int *point,*pointer; 
	static int temparr[1000];
	
	cout<< "Enter Number(1-1000): ";
	cin >> z;
	cout<< "\nCollatz Sequence: \n";
	cout << z << " ";
	
	point = collatz(z);
	for(int i=0;i<=1000;i++) {
		if(*(point+i)!=0)
		cout << *(point+i) << " ";
	}
	
	fibo(fib);
	cout << "\n\nFibonacci Sequence:" << endl;
	for(int i=0;i<=90;i++) {
		cout << fib[i] << endl;
	}
	
	system("pause");
	system("cls");

	node *head = NULL; 
	node *last = NULL; 
	char choice; 
	
	// dito sa part na ito, ichecheck muna ni user ang collatz sequence pagkatapos, ichecheck niya rin ang fibonacci
	// sequunce. Ngayon, kukuha si user ng fibonacci sequence galing sa collatz, iistore niya ito sa linked list, then
	// si user pwede niya ng idisplay yung mga number/data na nakita niya.
	
	
	
	// printing collatz sequence
	cout<< "\nCollatz Sequence: \n";
	cout << z << " ";
	for(int i=0;i<=1000;i++) {
		if(*(point+i)!=0)
		cout << *(point+i) << " ";
	}
	
	// printing fibonacci sequence
	cout << "\n\nFibonacci Sequence:" << endl;
	for(int i=0;i<=90;i++) {
		cout << fib[i] << endl;
	}
	
	cout << endl;
	do {
	choice = menu();
		switch(choice) 
		{
		case '1': 
			insert(head, last);
			break;
		case '2': 
			remove(head, last);
			break;
		case '3': 
			showList(head);
			break;
		case '4':
			cout << "Thank you for using this program! Have a great day!";
			exit(0);
			break;
		default:
			cout << "Error 404 not found! Please re-enter your choice.\n" << endl;
			break;
		}
	} while (choice != '4');
	return 0;
}
