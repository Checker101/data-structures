#include <iostream>
#include <stack>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <fstream>
using namespace std;
 
class Node {
  public:
    int key;
	string data;
	string artist;
	Node * next;

  Node() {
    key = 0;
    data;
    artist;
    next = NULL;
  }
  
  Node(int k, string d, string a) {
    key = k;
    data = d;
    artist = a;
  }
};

class Playlist {
  public:
    Node * head;
 
  Playlist() {
    head = NULL;
  }
  
  Playlist(Node * n) {
    head = n;
  }
 
  // 1st Check if node exists using key value
  Node * nodeExists(int k) {
    Node * temp = NULL;
    Node * ptr = head;
    while (ptr != NULL) {
      if (ptr -> key == k) {
        temp = ptr;
      }
      ptr = ptr -> next;
    }
    return temp;
  }
 
  // 2nd Insert a node to the list
  void insertNodefirst(Node * n) {
    if (nodeExists(n -> key) != NULL) {
      cout << "Node already exists with key value : " << "[" << n -> key << "]" << ", Insert another node with different Key value" << endl;
    } else {
      if (head == NULL) {
        head = n;
        cout << "Successfully added to the playlist!" << endl;
      } else {
        Node * ptr = head;
        while (ptr -> next != NULL) {
          ptr = ptr -> next;
        }
        ptr -> next = n;
        cout << "Successfully added to the playlist!" << endl;
      } 
    }
    system("pause");
    system("cls");
  }

  // 3rd Delete node by unique key/address/id
  void deleteNodeByKey(int k) {
    if (head == NULL) {
      cout << "\nThe playlist is already Empty. Can't delete :(" << endl;
    } 
	else if (head != NULL) {
      if (head -> key == k) {
        head = head -> next;
        cout << "Successfully deleted!" << k << endl;
      } else {
        Node * temp = NULL;
        Node * prevptr = head;
        Node * currentptr = head -> next;
        while (currentptr != NULL) {
          if (currentptr -> key == k) {
            temp = currentptr;
            currentptr = NULL;
          } else {
            prevptr = prevptr -> next;
            currentptr = currentptr -> next;
          }
        }
        if (temp != NULL) {
          prevptr -> next = temp -> next;
          cout << "Successfully deleted!" << k << endl;
        } else {
          cout << "Can't find that key. " << k << endl;
        }
      }
      
    }
  }
  // 4th update node (address, title, artist)
  void updateNodeByKey(int k, string d, string a) {
    Node * ptr = nodeExists(k);
    if (ptr != NULL) {
      ptr -> data = d;
      ptr -> artist = a;
      cout << "Successfully updated!" << endl;
    } else {
      cout << "Can't find that key." << k << endl;
    }
    system("pause");
    system("cls");
  }
 
  // 5th printing all the music
  void printList() {
    if (head == NULL) {
    	system("cls");
      cout << "The playlist is empty.";
    } else {
      cout << endl << "\t================MY PLAYLIST================" << endl;
      Node * temp = head; 
      
      while (temp != NULL) {
        cout << "\t(" << temp -> key << ", " << temp -> data << " by " << temp -> artist << ")" << endl; 
        temp = temp -> next;
      }
      cout << "\t===========================================";
    }
    cout << endl;
  }
  // 6th playing a song by unique key 
  void playNodeByKey(int k) {
  	if (head == NULL) {
		cout << "\nThe playlist is already Empty. Can't play a song :(" << endl;
	}
	Node * ptr = head;
	int i=1;
	while (ptr != NULL) {
		if (ptr -> key == k) {
			cout << "Now Playing: " << ptr -> data << " by " << ptr -> artist << endl;
			break;
		}
		else {
			ptr = ptr -> next;
		}
	}	i++;
}
	// function that will play a previous song
void playPrevious(int k){
	Node * ptr = head;
	while (ptr != NULL) 
	if (ptr -> key <= k-1) {
		cout << "Previous Song: " << ptr -> data << " by " << ptr -> artist << endl;
		break;
	} else {
		ptr = ptr -> next;
	}
}
	// function that will play a next song
void playNext(int k){
	Node * ptr = head;
	while (ptr != NULL)
	if (ptr -> key >= k+1){
		cout << "Next Song: " << ptr -> data << " by " << ptr -> artist << endl;
		break;
	} else {
		ptr = ptr -> next;
	}	
}


  
};

struct node {
	int key2;
	int top;
	string data;
	string artist1;
	string song;
	string artist2;
	node *prev;
};

bool isEmpty(node *head);
void insertAsFirstElement(node *&head, node *&last, string song, string artist2,int key2);
void Enqueue(node *&head, node *&last, node *current);
void Dequeue(node *&head, node *&last, string song);
void Print(node *current);

void insertElement(node *&head, node *&last, string data, string artist1,int top);
void Push(node *&head, node *&last, node *current);
void Pop(node *&head, node *&last);
void Display(node*current);

bool isEmpty(node *head) {
	if (head == NULL)
		return true;
	else 
		return false;
}

void insertAsFirstElement(node *&head, node *&last, string song, string artist2,int key2) {
	node *temp = new node;
	temp->key2 = key2;
	temp->song = song; 
	temp->artist2 = artist2;
	temp->prev = NULL;
	head = temp; 
	last = temp; 
}

void Enqueue(node *&head, node *&last, node *current) {
	system("CLS");
	int key2;
	string song;
	string artist2;
		cout << "\n\n\t\t=================MY PLAYLIST=================\n" << endl ;
		cout << "Front is here ------> ";
		while (current != NULL) {
			cout << "(" << current -> key2 << ", " << current -> song << " by " << current -> artist2 << ")\t" << endl;
			cout << "\n\t\t";
			current = current -> prev;
		} 
		cout << "\n\t\t=============================================" << endl;
		
	cout << "\n\n\t\t\tInserting Music \n\n"; 
	cout << "Enter an id/address of the song: ";
	cin >> key2;
	 
	cout << "Enter title of the song: ";
	getline (cin >> ws, song);
	
	cout << "Enter artist of the song: ";
	getline (cin >> ws, artist2);
	
	cout << "\n\nEnqueued successfully!" << endl;
	system("pause");
	system("CLS");
	
	if (isEmpty(head))
		insertAsFirstElement(head, last, song, artist2, key2);
		else {
			node *temp = new node;
			temp->key2 = key2;
			temp->song = song;
			temp->artist2 = artist2; 
			temp->prev = NULL;
			last->prev = temp;
			last = temp; 
		}
} 

void Dequeue(node *&head, node *&last) {
	if(isEmpty(head))
		cout << "Queue is empty! \n" << endl;
	if (head == last) {
		delete head; 
		head == NULL;
		last == NULL;
	} 
	else {
		node *temp = head;
		cout << "Front Dequeued!" << endl;
		head = head->prev;
		delete temp;
	}
	system("pause");
	system("cls");
} 

void Print(node *current) {
	system("cls");
	if(isEmpty(current)) 
		cout << "The playlist is empty!\n" << endl;	
	else {
		system("CLS");
		cout << "\n\n\t\t=================MY PLAYLIST=================\n" << endl ;
		cout << "Front is here ------> ";
		while (current != NULL) {
			cout << "(" << current -> key2 << ", " << current -> song << " by " << current -> artist2 << ")" << endl;
			cout << "\n\t\t";
			current = current -> prev;
		} 
		cout << "=============================================" << endl;
	}
	system("pause");
	system("cls");
}
void insertElement(node *&head, node *&last, string data, string artist1, int top) {
	node *temp = new node;
	temp->top = top;
	temp->data = data; 
	temp->artist1 = artist1;
	temp->prev = NULL;
	head = temp; 
	last = temp; 
}

void Push(node *&head, node *&last, node *current) {
	system("cls");
	int top;
	string data,artist1;
	cout << "\n\n\t\t=================MY PLAYLIST=================\n" << endl ;
		cout << "Top of stack is here ------> ";
		while (current != NULL) {
			cout << "(" << current -> top << ", " << current -> data << " by " << current -> artist1 << ")\t" << endl;
			cout << "\n\t\t";
			current = current -> prev;
		} 
		cout << "\n\t\t=============================================" << endl;
		
	cout << "\n\n\t\t\tInserting Music \n\n"; 
	cout << "Enter an id/address of the song: ";
	cin >> top;
	 
	cout << "Enter title of the song: ";
	getline (cin >> ws, data);
	
	cout << "Enter artist of the song: ";
	getline (cin >> ws, artist1);
	
	cout << "\n\nPush into Stack!" << endl;
	system("pause");
	system("CLS");
	
	if(isEmpty(head))
		insertElement(head, last, data, artist1, top);	
	else {
			node *temp = new node;
			temp->top = top;
			temp->data = data;
			temp->artist1 = artist1; 
			temp->prev = NULL;
			last->prev = temp;
			last = temp; 
	}
}

void Pop(node *&head, node *&last) {
	if (isEmpty(head))
		cout << "Stack is empty!" << endl;
	if (head == last) {
		delete last;
		head = NULL;
		last = NULL; 
	}
	else {
		node *temp = head;
		cout << "The top of stack is popped!" << endl;
		head = head->prev;
		delete temp;
	}
}

void Display(node*current){ 
	system("cls");
	if(isEmpty(current))
		cout << "Stack is empty!" << endl;
	else {
		system("cls");
		cout << "\n\n\t\t=================MY PLAYLIST=================\n" << endl ;
		cout << "Top of stack is here ------> ";
		while (current != NULL) {
			cout << "(" << current -> top << ", " << current -> data << " by " << current -> artist1 << ")\t" << endl;
			cout << "\n\t\t";
			current = current -> prev;
		} 
		cout << "\n\t\t=============================================" << endl;
	}
	system("pause");
	system("cls");
}

void input();
void searchsong();
void searchartist();
void searchalbum();
void searchgenre();
void quit();
void displayall();
void search();
void deleteFile();
void editFile();

void input() {       
 string song;
 string artist; 
 string album;
 string genre;
 ofstream newmusic("music playlist.txt", ios::app);
 system("cls");
 
 cout << "\n\n\t\t\tINERT YOUR MUSIC NOW" << endl;
 cout << "\n\nEnter the title of the song:";
 cin >> song;
 cin.sync();
 
 cout << "\n\nEnter the artist of the song:";
 cin >> artist;

 cout << "\n\nEnter the album of the song:";
 cin >> album;
 
 cout << "\n\nEnter the genre of the song:";
 cin >> genre;
 

 newmusic << song << " " << artist << " " << album << " " << genre << endl;     
 newmusic.close(); 

}

void searchsong() {
     ifstream music("music playlist.txt");
     string song;
     string str, line;
     string artist, offset;
     string album;
     string genre;
     system("CLS");
     cout << "Enter the title of the song:";
     cin >> str;
              
     while (music >> song >> artist >> album >> genre){  
           if (str == song){
     system ("CLS");
     cout << "Music found!" << endl;
     cout << "Title" << ' ' << "Artist" << ' ' << "Album" << ' ' << "Genre" << endl;
     cout << "---------------" << endl;     
     cout << song << ' '<< artist << ' ' << album << ' ' << genre << endl;
     }
    }

            
     while (music >> song ){
                 if (str != song){
                 system ("CLS") ;   
                 cout << "Music doesn't exists" << endl;     
                      
                      }
                      }
     system ("pause");
 }
 
void searchartist() {
     ifstream music("music playlist.txt");
     string song;
     string artist;
     string fartist;
     string album;
     string genre;
     system ("CLS");
     cout << "Enter the artist of a song:";
     getline (cin >> ws, fartist);
     
     while (music >> song >> artist >> album >> genre){
           if (fartist == artist){
                    system ("CLS");
                    cout << "Artist(s) found" << endl;
                    cout << "Title" << ' ' << "Artist" << ' ' << "Album" << ' ' << "Genre" << endl;
                    cout << "---------------" << endl;
                    cout << song << ' '<< artist << ' ' << album << ' ' << genre << endl;
                    }
                    }
     while (music >> artist){      
           if (fartist != artist){
                    system ("CLS");
                    cout << "No Artist(s) found"<< endl;
                    
                    
                    }  
           }
           system ("pause");
           cin.get();
  }
  
void searchalbum(){
     ifstream music("music playlist.txt");
     string song;
     string artist;
     string album;
     string falbum;
     string genre;
     system ("CLS");
     cout << "Enter an album's song:";
     getline (cin >> ws, falbum);
     
     while (music >> song >> artist >> album >> genre){
           if (falbum == album ){
                       system ("cls");
                       cout << "Album found"<< endl;
                       cout << "Title" << ' ' << "Artist" << ' ' << "Album" << ' ' << "Genre" << endl;
                       cout << "---------------" << endl;
                       cout << song << ' '<< artist << ' ' << album << ' ' << genre << endl;
                       
                       }
                       }
     while (music >> album){
            if (falbum != album){
                system ("CLS");
                cout << "No Album(s) found" << endl;
                }
           }
     system("pause");
     cin.get();
}

void searchgenre()
{
     ifstream music("music playlist.txt");
     string song;
     string artist;
     string album;
     string genre;
     string fgenre;
     system ("CLS");
     cout << "Enter a genre's song:";
     getline (cin >> ws, fgenre);
     
     while (music >> song >> artist >> album >> genre){
           if (fgenre == genre ){
                       system ("cls");
                       cout << "Genre found"<< endl;
                       cout << "Title" << ' ' << "Artist" << ' ' << "Album" << ' ' << "Genre" << endl;
                       cout << "---------------" << endl;
                       cout << song << ' '<< artist << ' ' << album << ' ' << genre << endl;
                       
                       }
                       }
     while (music >> genre){
            if (fgenre != genre){
                system ("CLS");
                cout << "No Genre(s) found" << endl;
                }
           }
     system("pause");
     cin.get();
 }
 
void quit()
{
 system ("CLS");
 cout << "Thank you for using database program" << endl;
 system ("pause");
 cin.get();
} 

void displayall()
{
     ifstream music("music playlist.txt");
     string song;
     string artist;
     string album;
     string genre;
     system ("CLS");
     
     cout << "\t\n\t==============ENTIRE MUSIC DATABASE==============="<< endl << endl;
 	 cout << "\tTitle" << "    ||    " << "Artist" << "    ||    " << "Album" << "    ||    " << "Genre" << endl;
     while (music >> song >> artist >> album >> genre){ 
     cout << "Title:" << song << endl;
     cout << "Artist:" << artist << endl;
     cout << "Album:" << album << endl;
     cout << "Genre:" << genre << endl << endl;
     }
     system ("pause");
     cin.get();
     
}

void search() {
     string artist;
     string song;
     string album;
     string genre;
     int choice2;
     int a;
     system ("CLS");
     
     cout << "\n\n\t\t\tSearch by category\n\n";
     cout << "\t[1]Search by title" << endl;
     cout << "\t[2]Search by artist" << endl;
     cout << "\t[3]Search by album" << endl;
     cout << "\t[4]Search by genre" << endl;
     cout << "\t[5]Display all music" << endl;
     cout << "\t[6]Back" << endl << endl;
     
     
     cout << "Enter your choice:";
     cin >> choice2;
     
     switch (choice2){
            case 1:
                 searchsong();
                 break;
            case 2:
                 searchartist();
                 break;
            case 3:
                 searchalbum();
                 break;
            case 4:
            	searchgenre();
            	break;
            case 5:
                 displayall();
                 break;
            case 6:
                cout << "\n\n\n\t*********************DATABASE MUSIC PLAYLIST*******************\n\n";
		        cout << "\t\t[1] Insert a music" << endl;
		        cout << "\t\t[2] Search database for music" << endl;
		        cout << "\t\t[3] Clear All data" << endl;
		        cout << "\t\t[4] Edit file" << endl;
		        cout << "\t\t[5] Exit Program" << endl << endl; 
		        cout << "\t***************************************************************" << endl << endl;
		       
		        cout << "Enter your choice:";
		        cin >> a;
                 break;
                }
    }
    
void deleteFile() {
     
     string decision;
     cout << "Are you sure?[Y]es[N]o" << endl;
     cin >> decision;
     if (decision == "y"){
     ofstream music("music playlist.txt");
     system("cls");
     cout << "Successfully Completed!" << endl;
     system("pause");
     music.close();
 
    }
}


void editFile()
{
 string song, input, newSong,decision;
 string artist,newArtist;
 string album,newAlbum;
 string genre,newGenre;
 ifstream music("music playlist.txt");
 if (!music.eof()){
music >> song >> artist >> album >> genre;
}
 system("cls");

 cout << "\t\n\t==============ENTIRE MUSIC DATABASE================"<< endl << endl;
 cout << "\tTitle" << "    ||    " << "Artist" << "    ||    " << "Album" << "    ||    " << "Genre" << endl;
     while (music >> song >> artist >> album >> genre){ 
     cout << "Title:" << song << endl;
     cout << "Artist:" << artist << endl;
     cout << "Album:" << album << endl;
     cout << "Genre:" << genre << endl << endl;
     }
     
 cout << "\n\n\t\tYou can now EDIT/REPLACE the music you want" << endl;
 cout << "Enter the title of the song:";
 getline (cin >> ws, input);

 if (input == song) {
     cout << song << " " << artist << " " << album << " " << genre << endl;
     cout << "Is this the correct music? [y][n]:";
     cin >> decision;
     
     if (decision == "y"){
     cout << "Enter the new title of the song:";
     getline (cin >> ws, newSong);
     song = newSong; 
	 
	 cout << "Enter the new artist of the song:";
     getline (cin >> ws, newArtist);
     artist = newArtist;
	 
	 cout << "Enter the new album of the song:";
     getline (cin >> ws, newAlbum);
     album = newAlbum;
	 
	 cout << "Enter the new genre of the song:";
     getline (cin >> ws, newGenre);
     genre = newGenre;  
	}        
     music.close();
}
    
     ofstream music2("music playlist.txt", ios::app);
     music2 << song << " " << artist << " " << album << " " << genre << endl;
     music2.close();
 system("pause");

}

int main() {
 
  Playlist s;
  int option,o,q,a;
  int choice;
  int key1, k1;
  string data1;
  string artist1;
  node *head = NULL; 
  node *last = NULL; 

  do {
  	cout << "\n\n\n\n\n\t\t\tChoose your choice HAHA\n\n" << endl;
	cout << "\t[1] Linked List" << endl;
	cout << "\t[2] Queue" << endl;
	cout << "\t[3] Stacks" << endl;
	cout << "\t[4] Database" << endl;
	cout << "\t[5] Exit" << endl << endl;
	cout << "\n\n\n\n\nEnter your choice: ";
	cin >> choice;
	
  	switch (choice) {
  		case 1: {
  			system("cls");
  			cout << "\n\n\n";
			cout << "\t*****************MUSIC PLAYER*********************\n";
			cout << "\t*                                               *";
			cout << "\n\t*     Select Option number. Enter 0 to exit.    *" << endl;
			cout << "\t*                                               *" ;
		    cout << "\n\t*  [1] Insert a music first                     *" << endl;
		    cout << "\t*  [2] Delete a music by id/address             *" << endl;
		    cout << "\t*  [3] Edit a music title                       *" << endl;
		    cout << "\t*  [4] Show all music                           *" << endl;
		    cout << "\t*  [5] Play music                               *" << endl;
		    cout << "\t*  [6] Back to Main Menu                        *" << endl << "\t*                                               *\n ";
		    cout << "\t*************************************************" << endl << endl;
		    cout << "Enter Option:";
		    cin >> option;
		    Node * n1 = new Node();
			
		    switch (option) {
		    case 0:
		      break;
		    case 1:
		    	system("cls");
		    	s.printList();
		      cout << "\n\n\t\t\tINSERTING A NODE " << endl; 
			  cout << "\tA node includes the following: (address,title,artist)" << endl << endl;
		      
		      cout << "Enter an address/id number of the music:" << endl;
		      cin >> key1;
		      cout << endl;
		      
		      cout << "Enter a title of the song:" << endl;
		      getline(cin >> ws, data1);
		      cout << endl;
		      
		      cout << "Enter the artist of the song you entered:" << endl;
		      getline(cin >> ws, artist1);
		      cout << endl << endl;
		      
		      n1 -> key = key1;
		      n1 -> data = data1;
		      n1 -> artist = artist1;
		      s.insertNodefirst(n1);
		      system("cls");
		      //cout<<n1.key<<" = "<<n1.data<<endl;
		      break;
		      
		      
		    case 2:
		    	system("cls");
		    	s.printList();
				cout << "\n\n\t\t\tDELETING A NODE " << endl;
				cout << "Enter an address of a song you want to delete:" << endl;
				cin >> k1;
				cout << endl;
				s.deleteNodeByKey(k1);
				system("pause");
				system("cls");
				break;
		      
		    case 3:
		    	system("cls");
		    	s.printList();
				cout << "\n\n\t\t\tUPDATING A NODE " << endl;
				cout << "Enter an address you want to update:" << endl;
				cin >> key1;
				cout << endl;
				
				cout << "Enter a new title of song:" << endl;
				getline(cin >> ws, data1);
				cout << endl;
				
				cout << "Enter the artist of the song you entered:" << endl;
				getline(cin >> ws, artist1);
				cout << endl << endl;
				s.updateNodeByKey(key1, data1, artist1);
				break;
		      
		    case 4:
				system("cls");
				s.printList();
				system("pause");
				system("cls");
				break;
		    
		    case 5:
		    	system("cls");
		    	s.printList();
		    	
		    	cout << "\n\n\t\t\tPLAYING A NODE " << endl;
		    	cout << "Select an address/id number you want to play:" << endl;
		    	cin >> k1;
		    	
		    	cout << endl; 
		    	s.playNext(k1);
		    	s.playNodeByKey(k1);
		    	s.playPrevious(k1);
		    	cout << endl;
		    	
		    	system("pause");
				system("cls");
		    	break;
		    case 6:
		    	system("cls");
		    	cout << "\n\n\n\n\n\t\t\tChoose your choice HAHA\n\n" << endl;
				cout << "\t[1] Linked List" << endl;
				cout << "\t[2] Queue" << endl;
				cout << "\t[3] Stacks" << endl;
				cout << "\t[4] Database" << endl;
				cout << "\t[5] Exit" << endl << endl;
				cout << "\n\n\n\n\nEnter your choice: ";
				cin >> choice;
		      	break;
		      	
		    default:
		      cout << "Error 404! Please enter a proper option. " << endl;
			} //Node n1;
			break;
		}
			
		case 2: {
			system("cls");
  			cout << "\n\n\n\t\t*****************************************\n";
			cout << "\t\t\t\tQUEUE ONLY\n\n\n";
			cout << "\t\t[1] EnQue\n";
			cout << "\t\t[2] DeQue\n";
			cout << "\t\t[3] Display all music\n";
			cout << "\t\t[4] Back to Main Menu\n";
			cout << endl;
			cout << "\t\t*****************************************\n";
			
			cout << "Enter Option: ";
			cin >> o;
			
  			switch (o) {
  				case 1:
  					Enqueue(head,last,head);
  					break;
  					
  				case 2:
  					Dequeue(head, last);
  					break;
  					
  				case 3:
  					Print(head);
  					break;
  					
  				case 4:
  					system("cls");
			    	cout << "\n\n\n\n\n\t\t\tChoose your choice HAHA\n\n" << endl;
					cout << "\t[1] Linked List" << endl;
					cout << "\t[2] Queue" << endl;
					cout << "\t[3] Stacks" << endl;
					cout << "\t[4] Database" << endl;
					cout << "\t[5] Exit" << endl << endl;
					cout << "\n\n\n\n\nEnter your choice: ";
					cin >> choice;
			  }
			break;
		}
		
		case 3: {
			system("cls");
			cout << "\n\n\n\t\t*****************************************\n";
			cout << "\t\t\t\tSTACKS ONLY\n\n\n";
			cout << "\t\t[1] Push\n";
			cout << "\t\t[2] Pop\n";
			cout << "\t\t[3] Display all music\n";
			cout << "\t\t[4] Back to Main Menu\n";
			cout << endl;
			cout << "\t\t*****************************************\n";
			
			cout << "Enter Option: ";
			cin >> q;
			
			switch (q) {
				case 1:
					Push(head,last,head);
					break;
				case 2:
					Pop(head,last);
					break;
				case 3:
					Display(head);
					break;
				case 4:
					system("cls");
			    	cout << "\n\n\n\n\n\t\t\tChoose your choice HAHA\n\n" << endl;
					cout << "\t[1] Linked List" << endl;
					cout << "\t[2] Queue" << endl;
					cout << "\t[3] Stacks" << endl;
					cout << "\t[4] Database" << endl;
					cout << "\t[5] Exit" << endl << endl;
					cout << "\n\n\n\n\nEnter your choice: ";
					cin >> choice;
					break;
			}
			break;
		}
  		case 4: {
  			system("cls");
  			cout << "\n\n\n\t*********************DATABASE MUSIC PLAYLIST*******************\n\n";
	        cout << "\t\t[1] Insert a music" << endl;
	        cout << "\t\t[2] Search database for music" << endl;
	        cout << "\t\t[3] Clear All data" << endl;
	        cout << "\t\t[4] Edit file" << endl;
	        cout << "\t\t[5] Back to Main menu" << endl << endl; 
	        cout << "\t***************************************************************" << endl << endl;
	       
	        cout << "Enter your choice:";
	        cin >> a;
	        
	        switch (a) {
	        	case 1:
	        		input();
	        		break;
	        	case 2:
		            search();
		            break;
		       case 3:
		            deleteFile();
		            break;
		       case 4:
		            editFile();
		            break;
		       case 5:
		            system("cls");
			    	cout << "\n\n\n\n\n\t\t\tChoose your choice HAHA\n\n" << endl;
					cout << "\t[1] Linked List" << endl;
					cout << "\t[2] Queue" << endl;
					cout << "\t[3] Stacks" << endl;
					cout << "\t[4] Database" << endl;
					cout << "\t[5] Exit" << endl << endl;
					cout << "\n\n\n\n\nEnter your choice: ";
					cin >> choice;
		            break;
		       		cin.get();       		
			}
			break;
		  }
		
		case 5:
			exit (0);
			break;	
		
}
 
  } while (option != 5);
 
  return 0;
}
